<?php
class Controllers_Add extends RestController {
	public function get() {
		//get an id of a lead and send as json 
		//not really nice becuase should be 
		// /lead/id/1234 instead will be lead/?id = 456
		
		if (isset($this->request['params']['id']){
			//build json and echo it 
			$this->responseStatus = 200;
		}else{
			$this->response = array('result' =>'Wrong parameters for Lead' );
			$this->responseStatus = 200;			
		}
	}
	public function post() {
		//get the json save it to the db and send the id as json
		$this->response = array('result' => 'no post implemented for Add');
		$this->responseStatus = 201;
	}
	public function put() {
		$this->response = array('result' => 'no put implemented for Add');
		$this->responseStatus = 200;
	}
	public function delete() {
		$this->response = array('result' => 'no delete implemented for Add');
		$this->responseStatus = 200;
	}
}
